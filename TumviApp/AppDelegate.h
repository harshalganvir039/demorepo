//
//  AppDelegate.h
//  TumviApp
//
//  Created by TacktileSystems MacThree on 10/06/15.
//  Copyright (c) 2015 TacktileSystems MacThree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

