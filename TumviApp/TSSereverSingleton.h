//
//  TSSereverSingleton.h
//  TumviApp
//
//  Created by TacktileSystems MacThree on 10/06/15.
//  Copyright (c) 2015 TacktileSystems MacThree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSSereverSingleton : NSObject

@end
